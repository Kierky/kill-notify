--[[
Kill Notifications v1.2.3
by Kierky.

Version 1.2.1/2/3: fixed %player% is having issues bug/minor ctc bot kill bug & color bug
Version 1.2: Added colourisation to player kills, and dulled bot kills.
Versions 1.0 and 1.1 were stabilisers, testing alphas, initial versions

--]]

-- Declare
kill = {}
kill.version = "1.2.3"
kill.record = {}
kill.event = {}
kill.notifier = {}
kill.togglenotify = {}

local weaponbase = {	[718]="Collision",
				[46]="Mega Positron",
				[742]="Capital Missile Turret",
				[213]="Turret-Port Gatling",
				[716]="Training Blaster",
				[173]="Sunflare", 
				[702]="Raven", 
				[44]="AAP", 
				[48]="LENB", 
				[34]="Neutron Blaster Mk II",
				[42]="Positron Blaster", 
				[183]="Seeker Missile", 
				[91]="Gov't Plasma Cannon", 
				[67]="Charged Cannon", 
				[114]="Plasma Cannon MkII", 
				[116]="Plasma Cannon MkIII", 
				[26]="Phase Blaster MkII", 
				[20]="Ion Blaster MkII", 
				[118]="Plasma Cannon HX", 
				[87]="Fletchette Cannon", 
				[32]="Neutron Blaster", 
				[89]="Fletchette Cannon MkII", 
				[103]="Gauss Cannon", 
				[156]="Rail Gun", 
				[105]="Gauss Cannon MkII", 
				[84]="Iceflare Rocket", 
				[158]="Rail Gun MkII", 
				[160]="Rail Gun MkIII", 
				[162]="Advanced Rail Gun",
				[86]="Starflare",
				[108]="Gemini Missile", 
				[708]="Plasma Eliminator", 
				[704]="Plasma Eliminator MkII", 
				[97]="Gatling Cannon", 
				[99]="Gatling Turret", 
				[147]="Plasma Devastator MkII", 
				[145]="Plasma Devastator", 
				[121]="Lightning Mine", 
				[136]="Stingray Missile", 
				[175]="Jackhammer Rocket", 
				[177]="Screamer Rocket", 
				[200]="Chaos Swarm Missile", 
				[18]="Ion Blaster", 
				[24]="Phase Blaster", 
				[22]="Ion Blaster MkIII", 
				[124]="Proximity Mine", 
				[198]="Locust Swarm Missile", 
				[28]="Orion Phase Blaster XGX", 
				[36]="Neutron Blaster MkIII", 
				[711]="Gauss Cannon MkIII", 
				[150]="Hive Positron Blaster", 
				[152]="Hive Gatling Cannon", 
				[134]="YellowJacket Missile", 
				[138]="Firefly Missile", 
				[128]="Tellar Ulam Mine", 
				[179]="Avalon Torpedo", 
				[706]="Plasma Annihilator", 
				[30]="TPG Sparrow Phase Blaster", 
				[40]="Corvus Widowmaker", 
				[56]="Capital Gauss", 
				[188]="Capital Gauss",
				[789]="Gatling Cannon HV",
				[747]="Firecracker Missile",
				[788]="Gemini Missile Turret",
				[193]="Aerna Explosives",
				[191]="Hull Explosion", 
				[0]="Self Destruct"
}

-- Variables
kill_notify_mode = true
--Colours
local red= "\127ff1010"
local white= "\127ffffff"
local green= "\12700cf10"
local blue= "\1270078ff"
local yellow= "\127cccc00"
local grey= "\127afafaf"
local customcolour1 = "\127ffffff"
local customcolour2 = "\127ffffff"
local customcolour3 = "\127ffffff"
local endcolour = "\127o"
-- Functions
function printmsg(colour, message)
	print(colour .. message .. endcolour)
end

function kill.togglenotify()
	if kill_notify_mode == true then
		kill_notify_mode = false
		printmsg(red,'Notify Mode Off')
	else
		kill_notify_mode = true
		printmsg(green,'Notify Mode On!')
	end
end

function kill.GetWeapon(number)
	local weapon = nil
	if weaponbase[number] ~= nil then
		weapon = weaponbase[number]
	else
		weapon = "Weapon Not Recorded: " .. number
	end
	return weapon
end

function kill.event:OnEvent(event, deadguy, killer, weapon)
	local killerchar = GetPlayerName(killer)
	local deadchar = GetPlayerName(deadguy)
	local weaponused = kill.GetWeapon(weapon)
	local killerfaction = GetPlayerFaction(killer)
	local deadguyfaction = GetPlayerFaction(deadguy)
	if ((string.sub(deadchar, 1, 1)~="*") and (string.sub(killerchar, 1, 1)~="*")) then
		-- if not a bot, do
		if killerfaction == 1 then
			customcolour1 = blue
		elseif killerfaction == 2 then
			customcolour1 = red
		else 
			customcolour1 = yellow
		end
		if deadguyfaction == 1 then
			customcolour2 = blue
		elseif deadguyfaction == 2 then
			customcolour2 = red
		else 
			customcolour2 = yellow
		end
		if killer == GetCharacterID() then
			customcolour3 = green
		else 
			customcolour3 = red
		end
	else
		customcolour1 = grey 
		customcolour2 = grey
		customcolour3 = grey
		-- this is a bot, play the death of the bot down
	end
	-- create different msgs
	local strippedmsg = killerchar .. " destroyed " .. deadchar .. " with " .. weaponused
	local oldmsg = killerchar .. " destroyed " .. deadchar
	local prettymsg = customcolour1 .. killerchar .. endcolour .. " destroyed " .. customcolour2 .. deadchar .. endcolour .. " with " .. customcolour3 .. weaponused .. endcolour
	local issuesmsg = customcolour1 .. killerchar .. endcolour .. " is having issues. (Killed themselves with " .. customcolour3 .. weaponused .. endcolour .. ")"
	if (kill_notify_mode == true and (killer ~= deadguy)) then
		printmsg(white,prettymsg)
	elseif (kill_notify_mode == true and (killer == deadguy)) then
		printmsg(white,issuesmsg)
	else
		chatreceiver:OnEvent("CHAT_MSG_DEATH", {msg=oldmsg})
	end
end
-- Events
RegisterEvent(kill.event, "PLAYER_DIED")
-- This is necessary to stop the system message from duplicating our one.
UnregisterEvent(chatreceiver, "CHAT_MSG_DEATH")

-- User Commands 
RegisterUserCommand("notify", kill.togglenotify)